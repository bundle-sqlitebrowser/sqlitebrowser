FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > sqlitebrowser.log'

COPY sqlitebrowser .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' sqlitebrowser
RUN bash ./docker.sh

RUN rm --force --recursive sqlitebrowser
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD sqlitebrowser
